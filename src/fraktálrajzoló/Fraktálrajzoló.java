/*
 * Copyright (C) 2017 Tamás Pisch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fraktálrajzoló;

import javax.swing.SwingUtilities;

/**
 *
 * @author Tamás Pisch
 */

public class Fraktálrajzoló {
  static FrModell fraktálModell = new FrModell();
  static FrAblak fraktálAblak;
  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        fraktálAblak = new FrAblak(fraktálModell);
        fraktálAblak.setFraktálAblak(fraktálAblak);
        fraktálModell.setFraktálAblak(fraktálAblak);//kiszedtem a fraktálModell konstruktorából, mert ott még nincs kész igazán...
        fraktálModell.alapértékek();
        fraktálAblak.jobbPanelAktualizálás();
        fraktálModell.setZoom(fraktálModell.getZoom()/100.0);
        fraktálAblak.setVisible(true);
      }
    });
    //Ez itt nem jó helyen van, de hogyan lehet korrekten megoldani??
    long idő=System.currentTimeMillis();
    //E nélkül nem jelenik meg a halmaz, csak az alapértékek gombra kattintva.
    //Ha valamiért lassan indul, akkor még így is jelentkezhet ez a probléma.
    while(System.currentTimeMillis()-idő<1000);
    fraktálModell.kezdőAnimáció();

  }
}