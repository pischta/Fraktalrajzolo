/*
 * Copyright (C) 2017 Tamás Pisch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fraktálrajzoló;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import org.w3c.dom.Element;

public class FrModell {
  private static FrAblak fraktálAblak;
  private ArrayList<String> fraktálTípusok;
  private String aktuálisFraktálTípus="Mandelbrot";
  private static int iteráció;
  private static double xZoom,yZoom;
  private static double xEltolás,yEltolás;
  static int paletta[]=new int[0xffffff];//ezt direktben érem el, mert nagy tömb, getterrel lassabb lenne
  static int palettaFeltöltésHatára=0;
  private Properties könyvjelzőParaméterek=new Properties();

  //Ez jelenleg egy felesleges plusz, mert a FrAblakból nem tudtam rá hivatkozni, legalábbis Stringként nem
  /*public enum BlurMódok {
    nincs ("Nincs"),
    egyszerű ("Egyszerű"),
    gauss ("Gauss");

    private final String name;       

    private BlurMódok(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false 
        return name.equals(otherName);
    }

    public String toString() {
       return this.name;
    }
  }
  private  static final String[] BLURTömb= {BlurMódok.nincs.toString(),BlurMódok.egyszerű.toString(),BlurMódok.gauss.toString()};*/
  private float gaussBlurMértéke;
  private static final ArrayList<String> palettaNevek=new ArrayList();
  private static final ArrayList<String> elmosásTípusok=new ArrayList();

  public static ArrayList<String> getPalettaNevek() {
    return palettaNevek;
  }

  public static ArrayList<String> getElmosásTípusok(){
    return elmosásTípusok;
  }
 
  public void setFraktálAblak(FrAblak fraktálAblak) {
    FrModell.fraktálAblak = fraktálAblak;
  }
  
  public String getAktuálisFraktálTípus(){
    return aktuálisFraktálTípus;
  }
  
  public void setAktuálisFraktálTípus(String típus){
    //ezt egyszerűbben is meg lehetne oldani, mivel tervek szerint csak 2 féle
    //fraktál lesz, de gondoltam  legyen a programban típusos vektor használat is
    if(!aktuálisFraktálTípus.equals(típus)){
      if(fraktálTípusok.contains(típus))
        aktuálisFraktálTípus=típus;
    }
  }

  public int getIteráció() {
    return iteráció;
  }

  public boolean setIteráció(int iteráció) {
    boolean siker=false;
    if(iteráció>0 && iteráció<=0xFFFFFF){
      this.iteráció = iteráció;
      siker=true;
      /*if(palettaFeltöltésHatára<iteráció)
        készítSzínPalettát(palettaFeltöltésHatára,iteráció);
      palettaFeltöltésHatára=iteráció;*/
      készítSzínPalettát(fraktálAblak.getPalettaTípus(), iteráció);
    }
    return siker;
  }

  public double getZoom() {
    return xZoom;
  }

  public void setZoom(double xZoom) {
    this.xZoom = xZoom;
    this.yZoom = xZoom;
  }

  public double getXEltolás() {
    return xEltolás;
  }

  public void setXEltolás(double xEltolás) {
    this.xEltolás = xEltolás;
  }

  public double getYEltolás() {
    return yEltolás;
  }
  
  public void setYEltolás(double yEltolás) {
    this.yEltolás = yEltolás;
  }

  public float getGaussBlurMértéke() {
    return gaussBlurMértéke;
  }

  public void setGaussBlurMértéke(float gaussBlurMértéke) {
    this.gaussBlurMértéke = gaussBlurMértéke;
  }
  
  
  public FrModell(){
    //fraktálAblak=fa;//jó lenne már itt tudni, de ez most még nem áll rendelkezésre!
    fraktálTípusok=new ArrayList<String>();
    fraktálTípusok.add("Mandelbrot");
    fraktálTípusok.add("Júlia");
    //"Paletta1","Paletta2","Paletta3"
    palettaNevek.add("Paletta1");
    palettaNevek.add("Paletta2");
    palettaNevek.add("Paletta3");
    elmosásTípusok.add("Nincs");
    elmosásTípusok.add("Egyszerű");
    elmosásTípusok.add("Gauss");
    //alapértékek();//ezt inkább az FrAblak konstruktorából hívom, mert most még nem tudom a fraktálterületMéretet
    //készítSzínPalettát();

  }
  
  public void alapértékek(){
    //xZoom=(4/3.0)*4/fraktálterületMéret.width;
    //xZoom=(1.0*fraktálterületMéret.width/fraktálterületMéret.height)*4.0/fraktálterületMéret.width;
    xZoom=4.0/fraktálAblak.getFraktálterületMéret().height;
    //yZoom=4.0/fraktálAblak.getFraktálterületMéret().height;
    yZoom=xZoom;
    xEltolás=-8/3.0;
    yEltolás=-2.0;
    setIteráció(255);
    setAktuálisFraktálTípus("Mandelbrot");
    //setBlur(true);
    fraktálAblak.setPalettaTípus("Paletta1");
    fraktálAblak.cbElmosás.setSelectedItem("Nincs");
    fraktálAblak.setTfElmosásMértéke("1.5");
    setGaussBlurMértéke(1.5f);
  }

  /**
   *  Az iterációt állítja be a paraméterek szerint, majd ezzel rajzoltat is.
   * @param kezdőIteráció 
   * @param végIteráció
   * @param lépésKöz 
   */
  private void animál2(int kezdőIteráció, int végIteráció, int lépésKöz){
    long számolásBefejezve=0;
    for (int i = kezdőIteráció; i <= végIteráció; i+=lépésKöz) {
      while((System.currentTimeMillis()-számolásBefejezve<100) ||
              (MandelbrotSzámoló.isFoglalt()));
      System.out.println("időköz: "+(System.currentTimeMillis()-számolásBefejezve));
      számolásBefejezve=System.currentTimeMillis();
      setIteráció(i);
      fraktálAblak.setIteráció(i);
      MandelbrotSzámoló.init(this, fraktálAblak, fraktálAblak.getFraktálterületMéret().width,
              fraktálAblak.getFraktálterületMéret().height,"");
    }
  }
  
  /**
   * Animációt valósít meg induláskor. Az iterációt növeli változó lépésközzel.
   * Valójában ez volt az első verzió.
   */
  public void kezdőAnimáció2(){
    animál2(1,20,1);
    animál2(25,100,5);
    animál2(115,255,20);
  }

  /**
   * Kezdő animáció. A halmazra közelít rá. 
   */
  public void kezdőAnimáció(){
    long előzőIdőpont=System.currentTimeMillis();
    double zoomCél=4.0/fraktálAblak.getFraktálterületMéret().height;
    double xCél=xEltolás;
    double yCél=yEltolás;
    double origóX=fraktálAblak.getFraktálterületMéret().width/2.0;
    double origóY=fraktálAblak.getFraktálterületMéret().height/2.0;
    setIteráció(50);//Hogy gyengébb gépeken is folyamatos lehessen. A teljes
      //halmaz megjelenítésekor még nem okoz látványos minőségromlást.
    fraktálAblak.setIteráció(50);
    int fázisokSzáma=100;
    for(int fázis=1;fázis<=fázisokSzáma;fázis++) {
      while(System.currentTimeMillis()-előzőIdőpont<20||
              (MandelbrotSzámoló.isFoglalt()));
      előzőIdőpont=System.currentTimeMillis();
      setZoom(zoomCél/(Math.sin(Math.toRadians(90*(1.0*fázis/fázisokSzáma)))));
      fraktálAblak.setZoom(getZoom());
      xEltolás=xCél-origóX*(1-Math.sin(Math.toRadians(90*(1.0*fázis/fázisokSzáma))))*getZoom();
      fraktálAblak.setTfXEltolás(xEltolás);
      yEltolás=yCél-origóY*(1-Math.sin(Math.toRadians(90*(1.0*fázis/fázisokSzáma))))*getZoom();
      fraktálAblak.setTfYEltolás(yEltolás);
      if (fázis==fázisokSzáma) {
        setIteráció(255);//Beállítom az animáció utáni kezdeti, vagy alapértelmezett értéket,
          //és ezzel számoltatom az utolsó fázist
        fraktálAblak.setIteráció(255);
      }
      MandelbrotSzámoló.init(this,fraktálAblak, fraktálAblak.getFraktálterületMéret().width,
                    fraktálAblak.getFraktálterületMéret().height,"");
    }
  }
  
  /**
   * Színpalettát készít. Ha egyszerűen az értékek lennének az színek, jellemzően
   * egy szín árnyalatai látszanának. Ennek egy módosított változata a Paletta3.
   * Nem számolom ki egyszerre az egész palettát, mert felesleges,
   * nagyon nagy iterációt nem jellemző, hogy beállítunk.
   * @param kezdet A kezdet értékéig már ki van számolva a paletta.
   * @param vég Ez a legnagyobb iteráció értéke, eddig számoltatok.
   */
  public void készítSzínPalettát(String palettaNév, int vég){
    //int mem[]=new int[800*600];
    int r,g,b;
/*    for (int j = 0; j < 120; j++) {
      for (int i = 800*j; i < 800*(j+1); i++) {
        r=((int)(127*Math.sin(Math.toRadians(i/40+180))+128)+Integer.MIN_VALUE)<<16;
        g=((int)(127*Math.sin(Math.toRadians(i/45+180))+128)+Integer.MIN_VALUE)<<8;
        b=(int)(127*Math.sin(Math.toRadians(i/100))+128)+Integer.MIN_VALUE;
        mem[i+800*j*4]=r+g+b;
        mem[i+800*(j*4+1)]=r+g+b;
        mem[i+800*(j*4+2)]=r+g+b;
        mem[i+800*(j*4+3)]=r+g+b;
        mem[i+800*(j*4+4)]=r+g+b;
      }
    }*/
    switch(palettaNév){
      case "Paletta1": 
                        for (int i = 0; i <= vég; i++) {
                          r=((int)(127.0*Math.sin(Math.toRadians((double)i/0.75+180.0))+128)+Integer.MIN_VALUE)<<16;
                          g=((int)(127.0*Math.sin(Math.toRadians((double)i/0.45+330.0))+128)+Integer.MIN_VALUE)<<8;
                          b=(int)(127.0*Math.sin(Math.toRadians((double)i/0.3+30))+128)+Integer.MIN_VALUE;
                          paletta[i]=(r+g+b)|0xff000000;
                        }
                        break;
      case "Paletta2":
                        for (int i = 0; i <= vég; i++) {
                          r=((int)(127.0*Math.sin(Math.toRadians((double)i/0.99+180.0))+128)+Integer.MIN_VALUE)<<16;
                          g=((int)(127.0*Math.sin(Math.toRadians((double)i/0.80+150.0))+128)+Integer.MIN_VALUE)<<8;
                          b=(int)(127.0*Math.sin(Math.toRadians((double)i/0.71+190))+128)+Integer.MIN_VALUE;
                          paletta[i]=(r+g+b)|0xff000000;
                        }
                        break;
      case "Paletta3":
                        for (int i = 0; i <= vég; i++) {
                          paletta[i]=i*100+Integer.MIN_VALUE|0xff000000;
                        }
                        break;
    }
    //pnFraktál.rajzol(mem);
  }
  
  /**
   * A palettát rajzolja ki, csak teszt jelleggel szoktam kirajzoltatni a fraktál helyett.
   * @param szélesség
   * @param magasság 
   */
  public void számolMandelbrototX(int szélesség,int magasság){
    int mem[]=new int[szélesség*magasság];
    System.arraycopy(paletta, 0, mem, 0, 800*600);
    fraktálAblak.rajzol(mem,szélesség,magasság);
    ///pnFraktálMutató.rajzol(mem);
  }

  /**
   * A párbeszédablakban megadott fájlnévbe menti a paramétereket.
   * @param fájl Célfájl neve
   */
  public void mentKönyvjelzőt(String fájl){
    könyvjelzőParaméterek.setProperty("Ablak szelessege",""+fraktálAblak.getSize().width);
    könyvjelzőParaméterek.setProperty("Ablak magassaga",""+fraktálAblak.getSize().height);
    könyvjelzőParaméterek.setProperty("X kezdo pozicio",""+getXEltolás());
    könyvjelzőParaméterek.setProperty("Y kezdo pozicio",""+getYEltolás());
    könyvjelzőParaméterek.setProperty("Nagyitas",""+getZoom());
    //könyvjelzőParaméterek.setProperty("Y Nagyitas",""+fraktálModell.getyZoom());
    könyvjelzőParaméterek.setProperty("Iteracio",""+getIteráció());
    könyvjelzőParaméterek.setProperty("Fraktal tipus",""+getAktuálisFraktálTípus());
    boolean kiírható=true;
    if((new File(fájl).exists()))
      if(!fraktálAblak.felülírható(fraktálAblak, "A fájl létezik. Felülírható?", "Kérdés"))
        kiírható=false;
    if(kiírható){
        try(FileOutputStream könyvjelző=new FileOutputStream(fájl)) {
          könyvjelzőParaméterek.store(könyvjelző, "Fraktalrajzolo konyvjelzofajl");
        } catch(FileNotFoundException e2){
          fraktálAblak.üzenet(fraktálAblak, "Nem tudtam a megadott fájlba írni!", "Hiba", true);
        } catch (Exception e2) {
          fraktálAblak.üzenet(fraktálAblak, "Nem sikerült a kiírás!", "Hiba", true);
        }
    }
  }  
  
  /**
   * A párbeszédablakban megadott fájlból tölt vissza paramétereket.
   * @param fájl 
   */
  public void betöltKönyvjelzőt(String fájl){
    try(FileInputStream könyvjelző=new FileInputStream(fájl)) {
      könyvjelzőParaméterek.load(könyvjelző);
    } catch(FileNotFoundException e2){
      fraktálAblak.üzenet(fraktálAblak, "Nem találom a megadott fájlt!", "Hiba", true);
    } catch (Exception e2) {
      fraktálAblak.üzenet(fraktálAblak, "Nem sikerült a beolvasás!", "Hiba", true);
    }
    try{
      if(könyvjelzőParaméterek.getProperty("X kezdo pozicio")==null ||
              könyvjelzőParaméterek.getProperty("Y kezdo pozicio")==null ||
              könyvjelzőParaméterek.getProperty("Nagyitas")==null ||
              könyvjelzőParaméterek.getProperty("Iteracio")==null ||
              könyvjelzőParaméterek.getProperty("Fraktal tipus")==null){
        //A keret dimenzió meglétét nem ellenőrzöm. Ha nincs megadva, marad a jelenlegi méret.
        fraktálAblak.üzenet(fraktálAblak, "Hiányzó paraméter a könyvjelzőfájlban!", "Hiba", true);
      }else{
        
        setXEltolás(Double.parseDouble(könyvjelzőParaméterek.getProperty("X kezdo pozicio")));
        setYEltolás(Double.parseDouble(könyvjelzőParaméterek.getProperty("Y kezdo pozicio")));
        setZoom(Double.parseDouble(könyvjelzőParaméterek.getProperty("Nagyitas")));
        //fraktálrajzoló.setyZoom(Double.parseDouble(könyvjelzőParaméterek.getProperty("Y Nagyitas")));
        setIteráció(Integer.parseInt(könyvjelzőParaméterek.getProperty("Iteracio")));
        setAktuálisFraktálTípus(könyvjelzőParaméterek.getProperty("Fraktal tipus"));
        fraktálAblak.jobbPanelAktualizálás();
        if(könyvjelzőParaméterek.containsKey("Ablak szelessege")&&(könyvjelzőParaméterek.containsKey("Ablak magassaga")))
          try{
            fraktálAblak.változtatAblakméretet(
                    new Dimension(Integer.valueOf(könyvjelzőParaméterek.getProperty("Ablak szelessege")),
                            Integer.valueOf(könyvjelzőParaméterek.getProperty("Ablak magassaga"))));
          }catch(NumberFormatException e){
            //Ha nem jó az érték, nem állítom át az ablakméretet, és nem jelzek hibát.
          }
      }
    }catch(NumberFormatException e3){
      fraktálAblak.üzenet(fraktálAblak, "Hiba a könyvjelzőfájlban!", "Hiba", true);
    }
    MandelbrotSzámoló.init(this, fraktálAblak, fraktálAblak.getFraktálterületMéret().width,
            fraktálAblak.getFraktálterületMéret().height,"");
  }
  
  private IIOMetadataNode készítCsomópontot(String keyword, String value){
    IIOMetadataNode textEntry = new IIOMetadataNode("tEXtEntry");
    textEntry.setAttribute("keyword", keyword);
    textEntry.setAttribute("value", value);
    IIOMetadataNode text = new IIOMetadataNode("tEXt");
    text.appendChild(textEntry);
    return text;
  }

  /**
   * Az előre kiszámolt, és paraméterben megkapott képet teszi BufferedImage-be,
   * beállítás szerint elmossa.
   * @param rajzTömb
   * @return 
   */
  private BufferedImage képetKészítMentéshez(int[] rajzTömb){
    Image img=fraktálAblak.készítKépet(rajzTömb);
          BufferedImage bi = new java.awt.image.BufferedImage(
                  fraktálAblak.getFraktálterületMéret().width,
                  fraktálAblak.getFraktálterületMéret().height,
                  java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
          Graphics2D g2 = bi.createGraphics();
          //g2.drawImage(img, 0, 0, null);
    switch(fraktálAblak.getAktuálisElmosásTípus()){
      case "Egyszerű":  return fraktálAblak.egyszerűElmosás(g2, img, bi);
      case "Gauss":     BufferedImage biFiltered = new java.awt.image.BufferedImage(
                            fraktálAblak.getFraktálterületMéret().width,
                  fraktálAblak.getFraktálterületMéret().height, java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
                        return fraktálAblak.gaussElmosás(g2, biFiltered,img, bi);
      default:  g2.drawImage(img, 0, 0, null);//Ez elvileg csak akkor fut le, ha "Nincs" az opció
                        return bi;
    }
          
  }
  
  /**
   * A paraméterként kapott tömbben lévő képet kiírja képként a megadott fájlba.
   * A fájlnév kiterjesztéséből határozza meg a formátumot.
   * @param rajzTömb Kép adatai
   * @param fájlnév Célfájl neve
   */
  public void mentKépet(int[] rajzTömb, String fájlnév){
    if((new File(fájlnév).exists()))
      if(!fraktálAblak.felülírható(fraktálAblak, "A fájl létezik. Felülírható?", "Kérdés"))
        return;
    if(fájlnév.toUpperCase().matches(".*.JPE?G$")){
      //https://docs.oracle.com/javase/7/docs/api/javax/imageio/metadata/doc-files/jpeg_metadata.html
      float quality=1f;
      ImageWriter writer = ImageIO.getImageWritersByFormatName("jpg").next();
      ////ImageWriteParam writeParam = writer.getDefaultWriteParam();
      JPEGImageWriteParam jpegParams;
      //if (quality >= 0 && quality <= 1f) {
      jpegParams = (JPEGImageWriteParam) writer.getDefaultWriteParam();
      jpegParams.setCompressionMode(JPEGImageWriteParam.MODE_EXPLICIT);
      jpegParams.setCompressionType("JPEG");
      jpegParams.setCompressionQuality(quality);
      //}
      ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
      //adding metadata
      IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, jpegParams);
      Element root = (Element) metadata.getAsTree("javax_imageio_jpeg_image_1.0");
      Element jfif = (Element) root.getElementsByTagName("app0JFIF").item(0);
      jfif.setAttribute("Xdensity", Integer.toString(200));
      jfif.setAttribute("Ydensity", Integer.toString(200));
      jfif.setAttribute("resUnits", "1");
      try {
        metadata.setFromTree("javax_imageio_jpeg_image_1.0", root);
      } catch (IIOInvalidTreeException ex) {
        fraktálAblak.üzenet(fraktálAblak, "Hiba a jpeg kép metadata készítés közben!\nÉrvénytelen ág", "Hiba", true);
      }
      IIOMetadataNode commentEntry =new IIOMetadataNode("com");
      String megjegyzés="Fraktal tipus: "+getAktuálisFraktálTípus()+"\r\n";
      megjegyzés+="X kezdopozicio"+Double.toString(getXEltolás())+"\r\n";
      megjegyzés+="Y kezdopozicio"+Double.toString(getYEltolás())+"\r\n";
      megjegyzés+="Nagyitas"+Double.toString(getZoom())+"\r\n";
      megjegyzés+="Iteráció"+Double.toString(getIteráció())+"\r\n";
      commentEntry.setAttribute("comment", megjegyzés);
      root.getElementsByTagName("markerSequence").item(0).appendChild(commentEntry);
      try{
        metadata.mergeTree("javax_imageio_jpeg_image_1.0", root);
      } catch(IIOInvalidTreeException e){
        fraktálAblak.üzenet(fraktálAblak, "Hiba a jpeg kép metadata készítés közben!", "Hiba", true);
        System.err.println(e.getMessage());
      }
      File fájl=new File(fájlnév);
      try (FileOutputStream fos = new FileOutputStream(fájl)) {
        try(ImageOutputStream stream = ImageIO.createImageOutputStream(fos)){
          writer.setOutput(stream);
          /*Image img=fraktálAblak.készítKépet(rajzTömb);
          BufferedImage bi = new java.awt.image.BufferedImage(
                  fraktálAblak.getFraktálterületMéret().width,
                  fraktálAblak.getFraktálterületMéret().height,
                  java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
          Graphics2D g2 = bi.createGraphics();
          g2.drawImage(img, 0, 0, null);*/
          try {
            //writer.write(metadata, new IIOImage(bi, null, metadata), jpegParams);
            writer.write(metadata, new IIOImage(képetKészítMentéshez(rajzTömb), null, metadata), jpegParams);
          } catch (IOException ex) {
            fraktálAblak.üzenet(fraktálAblak, "Nem sikerült a kép kiírása!", "Hiba", true);
          }
        }
      } catch(FileNotFoundException fileNFE){
        fraktálAblak.üzenet(fraktálAblak, "Hiba: nem sikerült a fájl létrehozása!", "Hiba", true);
      } catch (IOException streamEx) {
        fraktálAblak.üzenet(fraktálAblak, "Nem sikerült a kimeneti bájtfolyam létrehozása!", "Hiba", true);
      }
    }else if(fájlnév.toUpperCase().matches(".*.PNG$")){
      //https://docs.oracle.com/javase/7/docs/api/javax/imageio/metadata/doc-files/png_metadata.html
      ImageWriter writer = ImageIO.getImageWritersByFormatName("png").next();
      ImageWriteParam writeParam = writer.getDefaultWriteParam();
      ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
      IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);

      IIOMetadataNode root = new IIOMetadataNode("javax_imageio_png_1.0");
      root.appendChild(készítCsomópontot("Fraktal tipus", getAktuálisFraktálTípus()));
      root.appendChild(készítCsomópontot("X kezdopozicio", Double.toString(getXEltolás())));
      root.appendChild(készítCsomópontot("Y kezdopozicio", Double.toString(getYEltolás())));
      root.appendChild(készítCsomópontot("Nagyitas", Double.toString(getZoom())));
      root.appendChild(készítCsomópontot("Iteráció", Double.toString(getIteráció())));
      try {
        metadata.mergeTree("javax_imageio_png_1.0", root);
      } catch (IIOInvalidTreeException ex) {
        System.err.println("Metaadat összefésülési hiba!");
      }
      File fájl=new File(fájlnév);
      try {
        FileOutputStream fos=new FileOutputStream(fájl);
        ImageOutputStream stream;
        stream = ImageIO.createImageOutputStream(fos);
        writer.setOutput(stream);
        /*Image img=fraktálAblak.készítKépet(rajzTömb);
        BufferedImage bi = new java.awt.image.BufferedImage(fraktálAblak.getFraktálterületMéret().width, 
                  fraktálAblak.getFraktálterületMéret().height, java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g2 = bi.createGraphics();
        g2.drawImage(img, 0, 0, null);*/
        try {
          writer.write(metadata, new IIOImage(képetKészítMentéshez(rajzTömb), null, metadata), writeParam);
        } catch (IOException ex) {
          fraktálAblak.üzenet(fraktálAblak, "Hiba: nem sikerült a kép kiírása!", "Hiba", true);
        }
        stream.close();
        fos.close();
      } catch(FileNotFoundException fileNFE){
        fraktálAblak.üzenet(fraktálAblak, "Hiba: nem sikerült a fájl létrehozása!", "Hiba", true);
      } catch (IOException streamEx) {
        fraktálAblak.üzenet(fraktálAblak, "Nem sikerült a kimeneti bájtfolyam létrehozása!", "Hiba", true);
      }
    }else{
      fraktálAblak.üzenet(fraktálAblak, "Csak jpeg és png kép mentése támogatott!", "Üzenet", false);
    }
  }
}

/**
 * Az osztály végzi a Mandelbrot halmaz kiszámolását
 * @author pt
 */
class MandelbrotSzámoló implements Runnable{
  //
  // https://www.tutorialspoint.com/java/java_multithreading.htm
  //http://beginnersbook.com/2013/03/multithreading-in-java/
  //
  private static boolean foglalt=false;
  private static final int MAGOKSZÁMA=Runtime.getRuntime().availableProcessors();
  private int szélesség;//a pnFraktál szélessége
  private int eltolás;
  private int magasság;//a pnFraktál magassága
  private int[] memSzelet;//minden szálnak 
  private static int mem[];
  private FrModell hívó;
  private FrAblak fraktálAblak;
  private int sorszám;
  private int elsőSor;
  private int utolsóSor;
  private String fájlnév;
  private static Thread[] szál=new Thread[MAGOKSZÁMA];
  private static int[][] threadSorIndexHatárok=new int[MAGOKSZÁMA][2];

  public static boolean isFoglalt() {
    return foglalt;
  }

  public int getSzélesség() {
    return szélesség;
  }

  public int getMagasság() {
    return magasság;
  }

  public void setMagasság(int magasság) {
    this.magasság = magasság;
  }

  public int[] getMemSzelet() {
    return memSzelet;
  }
  
  /**
   * Ez az eljárás a belépési pont: megkapja a szükséges paramétereket,
   * majd létrehozza a magok száma szerinti objektumokat.
   * @param hívó
   * @param fraktálAblak
   * @param szélesség
   * @param magasság
   * @param fájlnév 
   */
  public static void init(FrModell hívó, FrAblak fraktálAblak, int szélesség,int magasság, String fájlnév){
    if(foglalt)
      return;
    else
      foglalt=true;
    MandelbrotSzámoló[] számolók=new MandelbrotSzámoló[MAGOKSZÁMA];
    long számolásIndult = System.currentTimeMillis();
    mem=new int[szélesség*magasság];
    long számolásBefejeződött;
    int utolsóSor=-1;
    int maradék=magasság%MAGOKSZÁMA;
    for (int i = 0; i < MAGOKSZÁMA; i++) {
      int elsőSor=utolsóSor+1;
      threadSorIndexHatárok[i][0]=elsőSor;
      int extraSor=0;
      if(maradék>0){
        extraSor=1;
        maradék--;
      }
      utolsóSor=elsőSor+(int)(1.0/MAGOKSZÁMA*magasság-1+extraSor);
      threadSorIndexHatárok[i][1]=utolsóSor;
      számolók[i]=new MandelbrotSzámoló(hívó, szélesség, magasság,
              i, elsőSor, utolsóSor, fájlnév);
    }
    for (int i = 0; i < MAGOKSZÁMA; i++) {
      try {
        szál[i].join();
      } catch (InterruptedException ex) {
        System.err.println("A szál megszakadt!");
      }
    }
    int xx=0;
    for (int i = 0; i < MAGOKSZÁMA; i++) {
      int[] aktuálisMemSzelet=számolók[i].getMemSzelet();
      for (int j = 0; j <= (threadSorIndexHatárok[i][1]-threadSorIndexHatárok[i][0]); j++) {
        for (int k = 0; k < szélesség; k++) {
          xx=(threadSorIndexHatárok[i][0]+j)*szélesség+k;
          mem[xx]=aktuálisMemSzelet[j*szélesség+k];
        }
      }
    }
    számolásBefejeződött=System.currentTimeMillis();
    System.out.println("A halmaz számolás " + ((double) (számolásBefejeződött - számolásIndult) / 1000)+" másodpercig tartott");

    if(fájlnév.isEmpty())
      fraktálAblak.rajzol(mem,szélesség,magasság);
    else
      hívó.mentKépet(mem,fájlnév);
    foglalt=false;
  } 
  
  /**
   * Konstruktor. Minden objektum a kép egy részét számolja ki,
   * processzormag-számtól függően.
   * @param hívó
   * @param szélesség A teljes kép szélessége
   * @param magasság A számolandó sorok szám
   * @param sorszám
   * @param elsőSor Ettől a sortól kell számolni
   * @param utolsóSor Eddig a sorig kell számolni
   * @param fájlnév Ha nem "", akkor kép mentése a feladat
   */
  public MandelbrotSzámoló(FrModell hívó, int szélesség, 
          int magasság, int sorszám, int elsőSor, int utolsóSor, String fájlnév){
    this.hívó=hívó;
    this.szélesség=szélesség;
    this.magasság=magasság;
    this.sorszám=sorszám;
    this.elsőSor=elsőSor;
    this.utolsóSor=utolsóSor;
    this.eltolás=elsőSor;
    this.fájlnév=fájlnév;
    szál[sorszám]=new Thread(this);
    szál[sorszám].start();
  }
  
  @Override
  public void run() {
    memSzelet=new int[(szélesség*(utolsóSor-elsőSor+1))];
    //int blurred[]=new int[szélesség*magasság];
    double x,y,a,b,c;//,d,e;
    int iteráció=hívó.getIteráció();
    for (int aktuálisXKoordináta = 0; aktuálisXKoordináta < szélesség; aktuálisXKoordináta++) {
      for (int aktuálisYKoordináta = elsőSor; aktuálisYKoordináta <= utolsóSor; aktuálisYKoordináta++) {
        x=aktuálisXKoordináta*hívó.getZoom()+hívó.getXEltolás();//ar=xEltolás  ar+=(double)axk*(double)(xs/maxx);xs*=yZoom;
        //maxx:képszélesség
        //gx:yZoom  gx=xs/képszélesség
        //axk:nagyításhoz használt téglalap kezdőpontja
        //xs: ez a nagyítás ablakának szélessége lehetett
        y=aktuálisYKoordináta*hívó.getZoom()+hívó.getYEltolás();
        a=x;
        b=y;
        int w=0;
        while(w<iteráció && (a*a+b*b)<4){
            c=a*a-b*b+x;
            b=a*b*2.0+y;
            a=c;
            w++;
        }
        if(w==iteráció){
          memSzelet[aktuálisXKoordináta + (aktuálisYKoordináta-eltolás) * szélesség] =0;//Integer.MAX_VALUE;//15_000_000+Integer.MIN_VALUE;
        }
        else
          memSzelet[aktuálisXKoordináta + (aktuálisYKoordináta-eltolás) * szélesség] =FrModell.paletta[w];
          //mem[aktuálisXKoordináta + aktuálisYKoordináta * szélesség] =w*színPalettaArány+Integer.MIN_VALUE;//+Integer.MIN_VALUE;
      }
    }
    //számolásBefejeződött=System.currentTimeMillis();
    //System.err.println("A számolás " + ((double) (számolásBefejeződött - számolásIndult) / 1000)+" másodpercig tartott");
    //Elmosás(valamiért nem működik):
    /*double rgb;
    for (int aktuálisXKoordináta = 1; aktuálisXKoordináta < szélesség-1; aktuálisXKoordináta++) {
      for (int aktuálisYKoordináta = 1; aktuálisYKoordináta < magasság-1; aktuálisYKoordináta++) {
        rgb=(mem[aktuálisXKoordináta-1 + (aktuálisYKoordináta-1) * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta + (aktuálisYKoordináta-1) * szélesség]& 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta+1 + (aktuálisYKoordináta-1) * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta-1 + aktuálisYKoordináta * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta+1 + aktuálisYKoordináta * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta-1 + (aktuálisYKoordináta+1) * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta + (aktuálisYKoordináta+1) * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta+1 + (aktuálisYKoordináta+1) * szélesség] & 0x00FFFFFF)/9.0+
                (mem[aktuálisXKoordináta + aktuálisYKoordináta * szélesség] & 0x00FFFFFF)/9.0;
        //red=mem[aktuálisXKoordináta + (aktuálisYKoordináta) * (szélesség+2)]/2.0+ mem[aktuálisXKoordináta+1 + (aktuálisYKoordináta) * (szélesség+2)] /2.0;
        blurred[aktuálisXKoordináta + (aktuálisYKoordináta) * szélesség]=0xff000000 | (int)Math.round(rgb);
      }
    }*/
  }

}