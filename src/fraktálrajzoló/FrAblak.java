/*
 * Copyright (C) 2017 Tamás Pisch
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fraktálrajzoló;

import java.awt.*;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.MaskFormatter;
import com.jhlabs.image.GaussianFilter;//http://www.jhlabs.com/ip/filters/index.html
import javax.swing.JComboBox;

public class FrAblak extends JFrame implements ActionListener, KeyListener, 
        MouseListener, MouseMotionListener,ComponentListener{
  private FrModell fraktálModell;
  private FrAblak fraktálAblak;//ez is kell az egérkezelésnél indított threadban
  private JPanel pnEszköztár,pnFraktál;
  private static final Dimension ESZKÖZTÁRPANELMÉRET=new Dimension(225, 596);
  private JLabel lbFraktál=new JLabel();
  private JLabel lbSegítség=new JLabel();
  private JLabel lbFraktálTípusa=new JLabel("Fraktál típusa");
  private JLabel lbFraktálTípusaVáltozó=new JLabel("Mandelbrot");
  private JButton btSegítség=new JButton("?");
  private JSeparator spFraktálTípusa=new JSeparator();
  //private JSeparator sp4=new JSeparator();
  private JPanel pnFraktálválasztó=new JPanel(new GridBagLayout());
  private ButtonGroup bgFraktáltípusokCsoport=new ButtonGroup();
  private JRadioButton rbMandelbrot=new JRadioButton("Mandelbrot",true);
  private JRadioButton rbJúlia=new JRadioButton("Júlia");
  private JLabel lbJúliaParaméterek=new JLabel("Júlia paraméterek");
  private JSeparator spJúliaParaméterek=new JSeparator();
  private JPanel pnJúliaParaméterek=new JPanel(new GridBagLayout());
  private JLabel lbX1=new JLabel("X:");
  private JTextField tfJúliaX=new JTextField();
  private JLabel lbY1=new JLabel("Y:");
  private JTextField tfJúliaY=new JTextField();
  private JCheckBox cbJúliaElőnézet=new JCheckBox("Júlia előnézet", false);
  private JLabel lbEltolás=new JLabel("Kezdő pozíció");
  private JSeparator spEltolás=new JSeparator();
  private JPanel pnEltolás=new JPanel(new GridBagLayout());
  private JLabel lbX2=new JLabel("X:");
  private JTextField tfXEltolás=new JTextField();
  private JLabel lbY2=new JLabel("Y:");
  private JTextField tfYEltolás=new JTextField();
  private final String VALÓSKARAKTEREK="0123456789.-";
  private JLabel lbNagyítás=new JLabel("Nagyítás");
  private JSeparator spNagyítás=new JSeparator();
  private JTextField tfNagyítás=new JTextField();
  private final String NEMNEGATÍVVALÓSKARAKTEREK="0123456789.";
  private JLabel lbIteráció=new JLabel("Iteráció");
  private JSeparator spIteráció=new JSeparator();
  private JFormattedTextField tfIteráció;
  private JLabel lbPaletta=new JLabel("Paletta");
  private JSeparator spPaletta=new JSeparator();
  private JComboBox cbPaletta=new JComboBox(fraktálModell.getPalettaNevek().toArray());
  //private JTextField tfIteráció=new JTextField();
  private JLabel lbElmosás=new JLabel("Elmosás");
  private JSeparator spElmosás=new JSeparator();
  private JPanel pnElmosás=new JPanel(new GridBagLayout());
  public JComboBox cbElmosás=new JComboBox(fraktálModell.getElmosásTípusok().toArray());
  private JLabel lbElmosásMértéke=new JLabel("Sugara:");
  private JFormattedTextField tfElmosásMértéke;
  private JLabel lbKönyvjelzők=new JLabel("Könyvjelzők");
  private JSeparator spKönyvjelzők=new JSeparator();
  private JPanel pnKönyvjelzőkKezelése=new JPanel(new GridBagLayout());
  private JButton btKönyvjelzőkMentése=new JButton("Mentés");
  private JFileChooser fc=new JFileChooser();
  //private Properties könyvjelzőParaméterek=new Properties();
  private JButton btKönyvjelzőkVisszatöltése=new JButton("Betöltés");
  private JPanel pnKépMentése=new JPanel(new GridBagLayout());
  private JLabel lbKép=new JLabel("Kép");
  private JSeparator spKépMentése=new JSeparator();
  private JButton btKépMentése=new JButton("Kép mentése");
  private JPanel pnAlapértékek=new JPanel(new GridBagLayout());
  private JLabel lbAlapértelmezések=new JLabel("Alapértékek");
  private JSeparator spAlapértelmezések=new JSeparator();
  private JButton btAlapértelmezések=new JButton("Alapértékek visszaállítása");
  private Dimension keretMéret;
  //public static int panelszélesség=800;
  //public static int panelmagasság=600;
  private Dimension asztalMéret=Toolkit.getDefaultToolkit().getScreenSize();
  private static Dimension fraktálterületMéret;
  private static final Dimension MINFRAKTÁLTERÜLETMÉRET=new Dimension(800, 600);

  private Point egérPozíció=new Point();
  volatile private boolean egérBalgombLenyombva=false;
  volatile private boolean egérJobbgombLenyombva=false;
  volatile private boolean fut = false;
  private long előzőIdőpont;

  public boolean isPnFraktálAppeared(){
    return pnFraktál.isShowing();
  }
  
  public static Dimension getFraktálterületMéret() {
    return fraktálterületMéret;
  }

  public static void setFraktálterületMéret(Dimension fraktálterületMéret) {
    FrAblak.fraktálterületMéret = fraktálterületMéret;
  }

  public JTextField getTfJúliaX() {
    return tfJúliaX;
  }

  public JTextField getTfJúliaY() {
    return tfJúliaY;
  }
  
  public void setTfJúliaX(double x){
    tfJúliaX.setText(""+x);
  }

  public void setTfJúliaY(double y){
    tfJúliaY.setText(""+y);
  }
  
  public void setTfXEltolás(double x){
    tfXEltolás.setText(Double.toString(x));
  }
  
  public void setTfYEltolás(double y){
    tfYEltolás.setText(Double.toString(y));
  }
  
  public void setZoom(double zoom){
    tfNagyítás.setText(Double.toString(zoom));
  }
  
  public void setIteráció(int iteráció){
    tfIteráció.setText(Integer.toString(iteráció));
  }
  
  public String getPalettaTípus(){
    return cbPaletta.getSelectedItem().toString();
  }
  
  public void setPalettaTípus(String palettaTípus){
    if(fraktálModell.getPalettaNevek().contains(palettaTípus)){
      cbPaletta.setSelectedItem(palettaTípus);
    }
  }

  public String getAktuálisElmosásTípus() {
    return cbElmosás.getSelectedItem().toString();
  }

  public void setAktuálisElmosásTípus(String elmosásTípus){
      if(fraktálModell.getElmosásTípusok().contains(elmosásTípus)){
        cbElmosás.setSelectedItem(elmosásTípus);
      }
  }

  
  public String getTfElmosásMértéke() {
    return tfElmosásMértéke.getText();
  }

  public void setTfElmosásMértéke(String érték) {
    //Nem vizsgáltatom külön az értékét, mert a formázása megteszi
    this.tfElmosásMértéke.setText(érték);// setValue(érték);
  }

  public void setFraktálAblak(FrAblak fraktálAblak) {
    this.fraktálAblak = fraktálAblak;
  }
  
  /**
   * Konstruktor. Beállítja a főablak elrendezését, kiteszi a bal és jobb panelt.
   * @param fm Megkapja az adatmodell osztály címét, hogy használhassa azt.
   */
  public FrAblak(FrModell fm){
    fraktálModell=fm;
    //fraktálModell.setFraktálAblak(this);//ekkor még nem biztos a címe az objektumnak
    //fraktálModell.alapértékek();
    initFőablak();
    initPnFraktál();
    initPnEszköztár();
    GridBagConstraints cFraktál=new GridBagConstraints();
    cFraktál.fill=GridBagConstraints.BOTH;
    cFraktál.gridx=0;
    cFraktál.gridy=0;
    cFraktál.weighty=1;
    cFraktál.weightx=1;
    add(pnFraktál,cFraktál);
    //jobbPanelAktualizálás();//itt még nincsenek meg az alapértékek
    GridBagConstraints cEszköztár=new GridBagConstraints();
    cEszköztár.fill=GridBagConstraints.NONE;
    cEszköztár.gridx=1;
    cEszköztár.gridy=0;
    cEszköztár.insets=new Insets(0,5,0,5);
    add(pnEszköztár,cEszköztár);
    pack();//ezt neten olvastam megoldásként arra, hogy hogyan állítsam be a minimum méretét az ablaknak: előtte pack(). Nem segít.
    keretMéret=this.getSize();//getBounds().getSize();
    System.out.println("keretméret:"+keretMéret);
    setMinimumSize(keretMéret);//ezt itt lehet, hogy másképp kellene meghatározni
    addComponentListener(this);
  }
  
  /**
   * A program ablakának néhány fő praméterét állítja be
   */
  private void initFőablak(){
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setTitle("Fraktálrazjoló");
    setLocationRelativeTo(this);
    //setLayout(new GridLayout(1, 2));//első oszlop: fraktál; második oszlop: eszköztár
    setLayout(new GridBagLayout());
    fraktálterületMéret=new Dimension(MINFRAKTÁLTERÜLETMÉRET);//itt majd figyeltetni a képernyőméretet esetleg
    Dimension felhasználóiFelületMéret=new Dimension(fraktálterületMéret.width+200,
          fraktálterületMéret.height);
//    setBounds((asztalMéret.width-felhasználóiFelületMéret.width)/2,
//            (asztalMéret.height-felhasználóiFelületMéret.height)/2,
//            felhasználóiFelületMéret.width, felhasználóiFelületMéret.height);
    setLocation((asztalMéret.width-felhasználóiFelületMéret.width)/2,
            (asztalMéret.height-felhasználóiFelületMéret.height)/2);
    getContentPane().setSize(felhasználóiFelületMéret.width, felhasználóiFelületMéret.height);
    getContentPane().setMinimumSize(new Dimension(MINFRAKTÁLTERÜLETMÉRET.width+200,MINFRAKTÁLTERÜLETMÉRET.height)); 
    System.out.println("contentpane mérete első beállítás után: "+getContentPane().getSize());
  }

  /**
   * Az átadott paraméternek megfelelően beállítja az ablakméretet - ha megfelelő.
   * Könyvjelző visszatöltésekor, esetleg későbbiekben kép visszatöltésekor
   * hívódik meg.
   * @param ablakMéret 
   */
  public void változtatAblakméretet(Dimension ablakMéret){
    if((ablakMéret.width>(MINFRAKTÁLTERÜLETMÉRET.width+200)) && (ablakMéret.height>(MINFRAKTÁLTERÜLETMÉRET.height))){
      //Dimension felhasználóiFelületMéret=new Dimension(MINFRAKTÁLTERÜLETMÉRET.width+200,
      //    MINFRAKTÁLTERÜLETMÉRET.height);
      setBounds(getBounds().x,getBounds().y,
            ablakMéret.width, ablakMéret.height);
      this.setSize(ablakMéret);
      System.out.println("ablakmérest setsize után:"+this.getSize());
      //System.out.println("pnfraktál most: "+pnFraktál.getSize());
      //setFraktálterületMéret(pnFraktál.getSize());
    }
    //Az ablak méretének megváltozásakor ComponentEvent esemény jön létre, a többit ott kezelem.
    //ha nem jók a bemeneti értékek, akkor marad az eredeti
  }
  
  /**
   * Létrehozza az eszköztárat, a jobb panelt
   */
  private void initPnEszköztár(){
    pnEszköztár=new JPanel();
    pnEszköztár.setMinimumSize(ESZKÖZTÁRPANELMÉRET);
    pnEszköztár.setPreferredSize(ESZKÖZTÁRPANELMÉRET);
    pnEszköztár.setLayout(new GridBagLayout());
    int összOszlopSzám=4;
    int aktuálisY=0;
    //Fraktál típus kiválasztása, Segítség
    //A cCímkéket többször is használom, a gridy változtatásával
    GridBagConstraints cCímkék=new GridBagConstraints();
    cCímkék.gridx=0;cCímkék.gridy=aktuálisY;cCímkék.anchor=GridBagConstraints.LINE_START;
    cCímkék.weighty=1;cCímkék.insets=new Insets(15, 0,0,0);
    pnEszköztár.add(lbFraktálTípusa,cCímkék);
    GridBagConstraints c2=new GridBagConstraints();
    c2.gridx=összOszlopSzám-1;c2.gridy=aktuálisY++;c2.anchor=GridBagConstraints.LINE_END;//c2.insets=new Insets(0,40,2,0);
    pnEszköztár.add(btSegítség,c2);
    btSegítség.addActionListener(this);
    GridBagConstraints cSzeparátor=new GridBagConstraints();
    cSzeparátor.gridx=0;cSzeparátor.gridy=aktuálisY++;cSzeparátor.gridwidth=összOszlopSzám;
    cSzeparátor.weighty=1;cSzeparátor.weightx=1;cSzeparátor.insets=new Insets(1,0,2,0);
    cSzeparátor.fill=GridBagConstraints.HORIZONTAL;
    //spFraktáltípusaSzeparátor.setPreferredSize(new Dimension(200,1));
    spFraktálTípusa.setOrientation(SwingConstants.HORIZONTAL);
    pnEszköztár.add(spFraktálTípusa,cSzeparátor);
    //A cMinipanelekhez is többször fel lesz használva
    GridBagConstraints cMiniPanelekhez=new GridBagConstraints();
    cMiniPanelekhez.gridx=0;cMiniPanelekhez.gridy=aktuálisY++;
    cMiniPanelekhez.gridwidth=összOszlopSzám;cMiniPanelekhez.fill=GridBagConstraints.BOTH;
    //pnEszköztár.add(pnFraktálválasztó,cMiniPanelekhez);
    GridBagConstraints c4=new GridBagConstraints();
    c4.gridx=0;c4.gridy=aktuálisY++;c4.anchor=GridBagConstraints.LINE_END;
    c4.gridwidth=összOszlopSzám;
    pnEszköztár.add(lbFraktálTípusaVáltozó,c4);

    //Júlia paraméterek kijelzése. Egyelőre nem aktív.
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbJúliaParaméterek,cCímkék);
    c4.gridy=aktuálisY++;
    spJúliaParaméterek.setOrientation(SwingConstants.HORIZONTAL);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spJúliaParaméterek,cSzeparátor);
    cMiniPanelekhez.gridx=0;cMiniPanelekhez.gridy=aktuálisY++;
    cMiniPanelekhez.gridwidth=összOszlopSzám;cMiniPanelekhez.fill=GridBagConstraints.BOTH;
    pnEszköztár.add(pnJúliaParaméterek,cMiniPanelekhez);
    GridBagConstraints c6=new GridBagConstraints();
    c6.gridx=0;c6.gridy=0;c6.anchor=GridBagConstraints.FIRST_LINE_START;
    pnJúliaParaméterek.add(lbX1,c6);
    GridBagConstraints c7=new GridBagConstraints();
    c7.gridx=1;c7.gridy=0;c7.fill=GridBagConstraints.HORIZONTAL;c7.weightx=1;//c7.ipadx=70;
    tfJúliaX.setEnabled(false);
    pnJúliaParaméterek.add(tfJúliaX,c7);
    c6.gridx+=2;
    pnJúliaParaméterek.add(lbY1,c6);
    c7.gridx+=2;//c7.ipadx=0;
    tfJúliaY.setEnabled(false);
    pnJúliaParaméterek.add(tfJúliaY,c7);
    c4.gridy=1;
    cbJúliaElőnézet.setEnabled(false);
    pnJúliaParaméterek.add(cbJúliaElőnézet,c4);

    //Az X és Y eltolás/kezdőpozíció kijelzése
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbEltolás,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spEltolás,cSzeparátor);

    //Az X és Y címkéknek és szövegmezőknek egy külön panelt hozok létre
    cMiniPanelekhez.gridy=aktuálisY++;
    pnEszköztár.add(pnEltolás,cMiniPanelekhez);
    c6.gridx=0;
    pnEltolás.add(lbX2,c6);
    c7.gridx=1;
    tfXEltolás.setPreferredSize(new Dimension(10, 18));
    pnEltolás.add(tfXEltolás,c7);
    tfXEltolás.addKeyListener(this);
    c6.gridx+=2;
    pnEltolás.add(lbY2,c6);
    c7.gridx+=2;
    tfYEltolás.setPreferredSize(new Dimension(10, 18));
    pnEltolás.add(tfYEltolás,c7);
    tfYEltolás.addKeyListener(this);
    
    //Nagyítás kijelzése
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbNagyítás,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spNagyítás,cSzeparátor);
    GridBagConstraints c8=new GridBagConstraints();
    c8.gridx=0;c8.gridwidth=összOszlopSzám;c8.gridy=aktuálisY++;
    c8.anchor=GridBagConstraints.LAST_LINE_END;c8.fill=GridBagConstraints.HORIZONTAL;
    c8.insets=new Insets(0,10,0,0);
    pnEszköztár.add(tfNagyítás,c8);
    tfNagyítás.setPreferredSize(new Dimension(17, 18));
    tfNagyítás.addKeyListener(this);
   
    //Iteráció kijelzése
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbIteráció,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spIteráció,cSzeparátor);
    c8.gridy=aktuálisY++;c8.insets=new Insets(0,100,0,0);
    try{
      MaskFormatter egész8jegyű=new MaskFormatter("########");
      tfIteráció=new JFormattedTextField(egész8jegyű);
    }
    catch(ParseException e){
      ;
    }
    tfIteráció.setColumns(8);
    pnEszköztár.add(tfIteráció,c8);
    tfIteráció.addKeyListener(this);
    
    //Palettaválasztó kijelzése
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbPaletta,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spPaletta,cSzeparátor);
    GridBagConstraints c9=new GridBagConstraints();
    c9.gridx=0;c9.gridwidth=összOszlopSzám;c9.insets=new Insets(0,100,0,0);
    c9.anchor=GridBagConstraints.LAST_LINE_END;c9.fill=GridBagConstraints.HORIZONTAL;
    c9.gridy=aktuálisY++;
    pnEszköztár.add(cbPaletta,c9);
    cbPaletta.addActionListener(this);
    //Elmosás kijelzése
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbElmosás,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spElmosás,cSzeparátor);
    cMiniPanelekhez.gridy=aktuálisY++;
    pnEszköztár.add(pnElmosás,cMiniPanelekhez);
    GridBagConstraints c10=new GridBagConstraints();
    c10.gridy=2;c10.gridx=1;
    pnElmosás.add(cbElmosás,c10);
    cbElmosás.addActionListener(this);
    c10.gridx=3;c10.insets=new Insets(0,5,0,0);
    lbElmosásMértéke.setEnabled(false);
    pnElmosás.add(lbElmosásMértéke,c10);
    c10.gridx=4;
    try {
      MaskFormatter floatKétTizedessel=new MaskFormatter("#.##");
      tfElmosásMértéke=new JFormattedTextField(floatKétTizedessel);
    } catch (Exception e) {
      ;
    }
    tfElmosásMértéke.setText(Float.toString(fraktálModell.getGaussBlurMértéke()));//setValue("1.1");
    tfElmosásMértéke.setColumns(4);
    tfElmosásMértéke.setEnabled(false);
    pnElmosás.add(tfElmosásMértéke,c10);
    tfElmosásMértéke.addKeyListener(this);
    //Könyvjelzők kezelése
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbKönyvjelzők,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spKönyvjelzők,cSzeparátor);
    //A könyvjelzők mentéséhez is gyártok egy külön panelt:
    cMiniPanelekhez.gridy=aktuálisY++;
    pnEszköztár.add(pnKönyvjelzőkKezelése,cMiniPanelekhez);
    GridBagConstraints c11=new GridBagConstraints();
    c11.gridx=1;
    c11.insets=new Insets(0, 0,0,10);
    c11.gridy=0;
    pnKönyvjelzőkKezelése.add(btKönyvjelzőkMentése,c11);
    btKönyvjelzőkMentése.addActionListener(this);
    //c9.anchor=GridBagConstraints.FIRST_LINE_END;
    c11.gridx+=2;//c9.gridwidth=2;//
    c11.insets=new Insets(0, 10,0,0);
    pnKönyvjelzőkKezelése.add(btKönyvjelzőkVisszatöltése,c11);
    btKönyvjelzőkVisszatöltése.addActionListener(this);
    
    //Kép mentése
    cCímkék.gridy=aktuálisY++;//lbKép.setFont(new Font("Dialog", 0, 12));
    pnEszköztár.add(lbKép,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spKépMentése,cSzeparátor);
    cMiniPanelekhez.gridy=aktuálisY++;//cMiniPanelekhez.gridheight=2;
    pnEszköztár.add(pnKépMentése,cMiniPanelekhez);
    GridBagConstraints c12=new GridBagConstraints();//c12.gridwidth=összOszlopSzám;
    c12.insets=new Insets(0,90,0,0);
    c12.anchor=GridBagConstraints.LAST_LINE_END;c12.fill=GridBagConstraints.HORIZONTAL;
    c12.gridx=0;c12.gridy=0;
    pnKépMentése.add(btKépMentése,c12);
    btKépMentése.addActionListener(this);
    
    //Alapértelmezések visszaállítása. A kép mentése panelbe megy ez is.
    cCímkék.gridy=aktuálisY++;
    pnEszköztár.add(lbAlapértelmezések,cCímkék);
    cSzeparátor.gridy=aktuálisY++;
    pnEszköztár.add(spAlapértelmezések,cSzeparátor);
    cMiniPanelekhez.gridy=aktuálisY++;
    pnEszköztár.add(pnAlapértékek,cMiniPanelekhez);
    GridBagConstraints c13=new GridBagConstraints();
    c13.gridx=0;c13.gridy=0;c13.anchor=GridBagConstraints.CENTER;
    c13.fill=GridBagConstraints.HORIZONTAL;c13.insets=new Insets(5,0,5,0);
    pnAlapértékek.add(btAlapértelmezések,c13);
    btAlapértelmezések.addActionListener(this);
  }

  /**
   * Létrehozza a rajzterületet, a bal oldali panelt.
   */
  private void initPnFraktál(){
    pnFraktál=new JPanel(new GridBagLayout());
    pnFraktál.setPreferredSize(MINFRAKTÁLTERÜLETMÉRET);pnFraktál.setMinimumSize(MINFRAKTÁLTERÜLETMÉRET);
    GridBagConstraints gc=new GridBagConstraints();
    gc.fill=GridBagConstraints.BOTH;
    gc.gridx=0;
    gc.gridy=0;
    pnFraktál.add(lbFraktál,gc);
    lbFraktál.addMouseMotionListener(this);
    lbFraktál.addMouseListener(this);
    //System.out.println(javax.swing.SwingUtilities.isEventDispatchThread()?
    //        "A kód az event dispatch threaden fut.":"A thread nem az event dispatch threaden fut!");
  }
  
  /**
   * Az eszköztár értékeit lehet vele frissíteni a fraktálModellből
   */
  public void jobbPanelAktualizálás(){
      if(fraktálModell.getAktuálisFraktálTípus().equals("Mandelbrot"))
        rbMandelbrot.setSelected(true);
      else
        rbJúlia.setSelected(true);
      /*
       * Ide kell majd még a Júlia paraméterek alapértékre állítása!!!!!!
       */
      setTfXEltolás(fraktálModell.getXEltolás());
      setTfYEltolás(fraktálModell.getYEltolás());
      setZoom(fraktálModell.getZoom());
      setIteráció(fraktálModell.getIteráció());

  }
  
  /**
   * A paraméterként kapott képet elmossa, és eredményként visszaadja.
   * @param g2
   * @param img
   * @param bi
   * @return 
   */
  public  BufferedImage egyszerűElmosás(Graphics2D g2,Image img,BufferedImage bi){
    // Egyszerű blur:
      long blurSzámolásKezdet=System.currentTimeMillis();
      int radius = 1;
      int size = radius * 2 + 1;
      float weight = 1.0f / (size * size*2);
      float[] data = new float[size * size];

      for (int i = 0; i < data.length; i++) {
          data[i] = weight;
      }
      data[5]=10*weight;
      //Kernel kernel = new Kernel(size, size, data);
      //ConvolveOp op = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
      BufferedImageOp blurOp = new ConvolveOp(new Kernel(3, 3, data));
      //BufferedImage bbi =op.filter(bi, null);
      //g2 = bi.createGraphics();
      g2.drawImage(img, 0, 0, null);
      BufferedImage clone = blurOp.filter(bi, null);
    
      //lbFraktál.setIcon(new ImageIcon(clone));
      System.out.println("A blur számolása "+(System.currentTimeMillis()-blurSzámolásKezdet)/1000.0+"-ig tartott");
    return clone;
  }
  
  /**
   * A paraméterként kapott képet Gauss módszerrel elmossa, és
   * eredményként visszaadja.
   * @param g2
   * @param biFiltered
   * @param img
   * @param bi
   * @return 
   */
  public  BufferedImage gaussElmosás(Graphics2D g2, BufferedImage biFiltered, Image img,BufferedImage bi){
      long blurSzámolásKezdet=System.currentTimeMillis();
      //System.out.println("Elmosás mértéke: "+fraktálModell.getGaussBlurMértéke());
      //System.out.println("mező tartalma: "+tfElmosásMértéke.getValue()+"f");
      GaussianFilter gf=new GaussianFilter(fraktálModell.getGaussBlurMértéke());
      System.out.println("gaussR:"+fraktálModell.getGaussBlurMértéke());
      g2 = bi.createGraphics();
      g2.drawImage(img, 0, 0, null);
      gf.filter(bi, biFiltered);
      //lbFraktál.setIcon(new ImageIcon(biFiltered));
      System.out.println("A blur számolása "+(System.currentTimeMillis()-blurSzámolásKezdet)/1000.0+"-ig tartott");
    return biFiltered;
  }
  
  /**
   * A paraméterben kapott értékek alapján rajzol. Elmosást is végez, a
   * beállítások szerint.
   * @param rajzTömb
   * @param szélesség
   * @param magasság 
   */
  public void rajzol(int[] rajzTömb,int szélesség,int magasság){
    Graphics2D g2;
    Image img = createImage(new java.awt.image.MemoryImageSource(
            szélesség, magasság, rajzTömb, 0, szélesség));
    BufferedImage bi = new java.awt.image.BufferedImage(
            szélesség, magasság, 
            java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
    g2 = bi.createGraphics();

    //System.out.println("panel mérete rajzoláskor: "+pnFraktál.getSize());
    switch(cbElmosás.getSelectedItem().toString()){
      case "Nincs":     g2.drawImage(img, 0, 0, null);
                        lbFraktál.setIcon(new ImageIcon(bi));
                        break;
      case "Egyszerű":  lbFraktál.setIcon(new ImageIcon(egyszerűElmosás(g2, img, bi)));
                        break;
      case "Gauss":     BufferedImage biFiltered = new java.awt.image.BufferedImage(
                            szélesség, magasság, java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
                        lbFraktál.setIcon(new ImageIcon(gaussElmosás(g2, biFiltered,img, bi)));
                        break;
    }
    
    //g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    //g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    g2.dispose();
    revalidate();
  }
 
  /**
   * Kép mentéséhez kellett különszednem
   * @param rajzTömb
   * @return 
   */
  public Image készítKépet(int[] rajzTömb){
    return createImage(new java.awt.image.MemoryImageSource(
            getFraktálterületMéret().width, getFraktálterületMéret().height, 
            rajzTömb, 0, getFraktálterületMéret().width));
  } 
  
  /**
   * A kapott fájlnévtöredéket kiegészíti az aktuális időpopnttal, plusz
   * .ini kiterjesztéssel
   * @param fájlnév
   * @return 
   */
  private String fájlnévKiegészítő(String fájlnév){
      DateFormat dátumformázó=DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
      fájlnév+=dátumformázó.format(new Date())+".ini";
      fájlnév=fájlnév.replace(' ', '_');
      fájlnév=fájlnév.replace(':', '-');
      return fájlnév;
  }
  
  /**
   * 
   * @param ablakCím Dialógusablak címe.
   * @param kiterjesztés Pl.: ".INI" Pont is kell, és csupa nagybetűvel kell írni!
   * @param fájlnévKezdet Alapértelmezett fájlnév kezdete. A kiegészítést a metódus végzi. 
   * @param szűrésLeírás Pl.: "ini fájlok"
   */
  private void fájlPanelElőkészítő(String ablakCím,String  kiterjesztés,String fájlnévKezdet, String szűrésLeírás){
    fc.setCurrentDirectory(new File("."));
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setDialogTitle(ablakCím);
      fc.setMultiSelectionEnabled(false);
      fc.setFileFilter(new FileFilter() {
        @Override
        public boolean accept(File f) {
          return (f.isDirectory() || f.getName().toUpperCase().endsWith(kiterjesztés));
        }

        @Override
        public String getDescription() {
          return szűrésLeírás;
        }
      });
      //ha nincs jobb ötlet, akkor a felajánlott fájlnéven lehet menteni, ami
      //mindig (percenként) egyedi lesz, mert benne van a dátum+idő a névben
      String alapértelmezettFájlnév=fájlnévKiegészítő(fájlnévKezdet);
      fc.setSelectedFile(new File(alapértelmezettFájlnév));
  }
  
  /**
   * 
   * @param ablakCím Dialógusablak címe.
   * @param kiterjesztések Pl.: ".INI" Pont is kell, és csupa nagybetűvel kell írni!
   * @param szűrésLeírás Pl.: "ini fájlok"
   */
  private void fájlPanelElőkészítő(String ablakCím,ArrayList<String>  kiterjesztések, String szűrésLeírás){
    fc.setCurrentDirectory(new File("."));
      fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
      fc.setDialogTitle(ablakCím);
      fc.setMultiSelectionEnabled(false);
      fc.setFileFilter(new FileFilter() {
        @Override
        public boolean accept(File f) {
          boolean ok=false;
          for (String kiterjesztés : kiterjesztések) {
            if(f.getName().toUpperCase().endsWith(kiterjesztés.toUpperCase()))
              ok=true;
          }
          return (f.isDirectory() || ok);
        }

        @Override
        public String getDescription() {
          return szűrésLeírás;
        }
      });
      fc.setSelectedFile(new File(""));
  }

  /**
   * Gombokra kattintás figyelése.
   * @param e Keletkezett esemény objektuma
   */
  @Override
  public void actionPerformed(ActionEvent e){
    if(e.getSource()==btAlapértelmezések){
      setFraktálterületMéret(MINFRAKTÁLTERÜLETMÉRET);
      Dimension felhasználóiFelületMéret=new Dimension(MINFRAKTÁLTERÜLETMÉRET.width+200,
          MINFRAKTÁLTERÜLETMÉRET.height);
      setBounds(getBounds().x,getBounds().y,
            felhasználóiFelületMéret.width, felhasználóiFelületMéret.height);
      fraktálModell.alapértékek();
      jobbPanelAktualizálás();
//      if(fraktálModell.getAktuálisFraktálTípus().equals("Mandelbrot"))
//        rbMandelbrot.setSelected(true);
//      else
//        rbJúlia.setSelected(true);
//      /*
//       * Ide kell majd még a Júlia paraméterek alapértékre állítása!!!!!!
//       */
      MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
    }else if(e.getSource()==btKönyvjelzőkMentése){
      //könyvjelzők mentésére kattintott
      //előkészítem a fájlkiválasztó dialógusablakot
      fájlPanelElőkészítő("Könyvjelző mentése", ".INI", "Fraktálrajzoló_könyvjelző_","ini fájlok");
      if(fc.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){
        System.out.println("kiválaszott fájl: "+fc.getSelectedFile().getPath());
        fraktálModell.mentKönyvjelzőt(fc.getSelectedFile().getPath());
      }
    }else if(e.getSource()==cbPaletta){
      System.out.println("paletta változtatás");
      fraktálModell.készítSzínPalettát(cbPaletta.getSelectedItem().toString(), fraktálModell.getIteráció());
      MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
    }else if(e.getSource()==cbElmosás){
      if(cbElmosás.getSelectedItem().toString().equals("Nincs")){
        lbElmosásMértéke.setEnabled(false);
        tfElmosásMértéke.setEnabled(false);
      }else if(cbElmosás.getSelectedItem().toString().equals("Egyszerű")){
        lbElmosásMértéke.setEnabled(false);
        tfElmosásMértéke.setEnabled(false);
      }else if(cbElmosás.getSelectedItem().toString().equals("Gauss")){
        lbElmosásMértéke.setEnabled(true);
        tfElmosásMértéke.setEnabled(true);
        tfElmosásMértéke.setText(Float.toString(fraktálModell.getGaussBlurMértéke()));
      }
      while(MandelbrotSzámoló.isFoglalt());
      MandelbrotSzámoló.init(fraktálModell, fraktálAblak,
                  pnFraktál.getSize().width,pnFraktál.getSize().height, "");
    }else if(e.getSource()==btKönyvjelzőkVisszatöltése){
      //könyvjelzők mentésére kattintott
      //előkészítem a fájlkiválasztó dialógusablakot
      ArrayList<String> kiterjesztések=new ArrayList<>();
      kiterjesztések.add(".ini");
      fájlPanelElőkészítő("Könyvjelző visszatöltése", kiterjesztések, "ini fájlok");
      if(fc.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
        fraktálModell.betöltKönyvjelzőt(fc.getSelectedFile().getPath());
      }
      
    }else if(e.getSource()==btKépMentése){
      ArrayList<String> kiterjesztések=new ArrayList<>();
      kiterjesztések.add(".jpg");
      kiterjesztések.add(".png");
      fájlPanelElőkészítő("Kép mentése", kiterjesztések, "jpg, png fájlok");
      if(fc.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){
        MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,fc.getSelectedFile().getPath());
      }
    }else if(e.getSource()==btSegítség){
        GridBagConstraints gc=new GridBagConstraints();
        gc.fill=GridBagConstraints.BOTH;
        gc.gridx=0;
        gc.gridy=0;

      if(lbFraktál.isShowing()){
        pnFraktál.remove(lbFraktál);
        //pnFraktál.setOpaque(true);
        pnFraktál.setBackground(new Color(0,0,20));
        lbSegítség.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
        pnFraktál.setForeground(new Color(0, 0, 100));//a0522d
        lbSegítség.setText(
            "<html><body bgcolor='#000020' text='#cf6a3a'>"
          + "<center><em>A programról</em></center>"
          + "<br />"
          + "<p align='justify'>Ezt a programot tanulási célból írtam, de talán másnak is hasznos, érdekes lesz.</p>"
          + "<p align='justify'>Jelenleg csak a Mandelbrot halmazt tudja kirajzolni. Ez a halmaz törtdimenziójú, önhasonló.</p>"
          + "<br/>"
          + "<p align='justify'>A program kezelése egyszerű. Bal egérgombbal lehet egy adott pontra nagyítani folyamatosan, jobb egérgombbal pedig kicsinyíteni.</p>"
          + "<p align='justify'>A paramétereket kézzel is lehet állítani a jobb oldali panelen. Nagyobb nagyításnál érdemes az iterációt nagyobbra állítani, mert részletgazdagabb lesz a kép.</p>"         
          + "<p align='justify'>El lehet menteni az aktuális képet, amiben a kép paraméterei szerepelnek, hasonlóan a mentett könyvjelzőhöz. Elvileg képnéző programok is megjeleníthetik ezeket a paramétereket, de későbbi megvalósításként képből is vissza lehetne tölteni a paramétereket a programba.</p>"
          + "</body></html>");
        pnFraktál.add(lbSegítség);
        pack();
        lbSegítség.setPreferredSize(MINFRAKTÁLTERÜLETMÉRET);
        pnFraktál.repaint();
      }else{
        pnFraktál.remove(lbSegítség);
        pnFraktál.add(lbFraktál,gc);
        pack();
        //pnFraktál.setMinimumSize(MINFRAKTÁLTERÜLETMÉRET);
        pnFraktál.repaint();
      }
    }
  }

  /**
   * Billentyű leütésekor, annak felengedése után keletkező esemény figyelése, kezelése.
   * @param e Keletkezett esemény objektuma.
   */
  @Override
  public void keyTyped(KeyEvent e){
    ;
  }

  /**
   * Billentyű lenyomásakor keletkező esemény figyelése, kezelése
   * @param e Keletkezett esemény objektuma.
   */
  @Override
  public void keyPressed(KeyEvent e){
    String megengedettKarakterek;
    //beállítom a megengedett karaktereket, attól függően, melyik szövegmezőben
    //volt leütés
    if(e.getSource()==tfNagyítás)
      megengedettKarakterek=NEMNEGATÍVVALÓSKARAKTEREK;
    else if((e.getSource()==tfXEltolás) || (e.getSource()==tfYEltolás)||
            (e.getSource()==tfJúliaX)||(e.getSource()==tfJúliaY))
      megengedettKarakterek=VALÓSKARAKTEREK;
    else
      megengedettKarakterek=VALÓSKARAKTEREK;//ez itt változhat!!!!
    //Elvileg a VK_LEFT és a VK_RIGHT a jobbra/balra nyilakat jelenti, mégis
    //csak a 37 és 39-es kódokkal tudom figyeltetni
    if((megengedettKarakterek.indexOf(e.getKeyChar())==-1)&&
            (e.getKeyChar()!=KeyEvent.VK_DELETE)&&
            (e.getKeyChar()!=KeyEvent.VK_BACK_SPACE)&&
            (e.getKeyChar()!=KeyEvent.VK_ENTER)&&
            (e.getKeyChar()!=KeyEvent.VK_LEFT)&&
            (e.getKeyChar()!=KeyEvent.VK_RIGHT)&&
            (e.getKeyCode()!=37)&&
            (e.getKeyCode()!=39)&&
            (e.getKeyChar()!=KeyEvent.VK_ENTER)){
      ((JTextField)e.getSource()).setEditable(false);
    }
    //ha entert ütöttünk
    if(e.getKeyCode()==10){
      if(e.getSource()==tfIteráció){
        String iterációMező=tfIteráció.getText().trim();
        try{
          if(fraktálModell.setIteráció(Integer.parseInt(tfIteráció.getText().trim())))
            MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
          else
            tfIteráció.setEditable(false);
        }catch(NumberFormatException ex){
          tfIteráció.setEditable(false);
        }
      }else if(e.getSource()==tfNagyítás){
        if(isDouble(tfNagyítás.getText().trim())){
          fraktálModell.setZoom(Double.parseDouble(tfNagyítás.getText().trim()));
          MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
        }
        else{
          tfNagyítás.setEditable(false);
        }
      }else if(e.getSource()==tfXEltolás){
        if(isDouble(tfXEltolás.getText().trim())){
          fraktálModell.setXEltolás(Double.parseDouble(tfXEltolás.getText().trim()));
          MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
        }else
          tfXEltolás.setEditable(false);
      }else if(e.getSource()==tfYEltolás){
        if(isDouble(tfYEltolás.getText().trim())){
          fraktálModell.setYEltolás(Double.parseDouble(tfYEltolás.getText().trim()));
          MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
        }else
          tfYEltolás.setEditable(false);
      }else if(e.getSource()==tfElmosásMértéke){
        if(isDouble(tfElmosásMértéke.getText().toString().trim())){
          fraktálModell.setGaussBlurMértéke((float)Double.parseDouble(tfElmosásMértéke.getText().toString().trim()));
          MandelbrotSzámoló.init(fraktálModell, this, getFraktálterületMéret().width,
              getFraktálterületMéret().height,"");
        }else  
          tfElmosásMértéke.setEditable(false);
      }
    }
  }

  /**
   * A függvény megvizsgálja, hogy a paraméterben kapott string double típusú
   * számmá alakítható-e
   * @param str Vizsgálandó sztring
   * @return true-val tér vissza, ha a sztring Double típusúvá alakítható
   */
  private boolean isDouble(String str){
    try{  
      double d = Double.parseDouble(str);  
    }  
    catch(NumberFormatException nfe){  
      return false;  
    }  
    return true;  
  }
  
  /**
   * Dialógusablakot jelenít meg, a paraméterként átadott üzenettel.
   * @param szülő Keret mutatója
   * @param üzenet Kiírandó üzenet
   * @param cím Üzenetablak címe.
   * @param hiba Ha ez true, akkor hibajelzés, ha false, akkor "egyszerű" üzenet típusú
   */
  public void üzenet(JFrame szülő, String üzenet, String cím, boolean hiba){
    int típus;
    if(hiba)
      típus=JOptionPane.ERROR_MESSAGE;
    else
      típus=JOptionPane.INFORMATION_MESSAGE;
    JOptionPane.showMessageDialog(szülő, üzenet, cím, típus);
  }
  
  public boolean felülírható(JFrame szülő, String kérdés, String cím){
    int típus=JOptionPane.YES_NO_OPTION;
    Object[] opciók={"Igen","Nem"};
    int vissza=JOptionPane.showOptionDialog(szülő, new String(kérdés), cím, típus, JOptionPane.QUESTION_MESSAGE , null,opciók ,"Nem");
    return vissza==0;
  }
  
  /**
   * Billentyű elengedésekor keletkező esemény lekezelése
   * @param e Keletkező esemény objektuma
   */
  @Override
  public void keyReleased(KeyEvent e){
    ((JTextField)e.getSource()).setEditable(true);
  }

  /**
   * Egérgomb lenyomva tartása mellett elmozdult, azaz változott a mutató pozíciója
   * @param e Keletkező esemény objektuma.
   */
   @Override
  public void mouseDragged(MouseEvent e) {
    egérPozíció=e.getPoint();
  }

  /**
   * Egérkurzor pozíciója megváltozott
   * @param e Keletkező esemény objektuma.
   */
  @Override
  public void mouseMoved(MouseEvent e) {
    egérPozíció=e.getPoint();
  }

  /**
   * Egérkattintás lekezelése. Akkor keletkezik ez az esemény, ha a lenyomás
   * után felengedjük az egérgombot.
   * @param e Keletkező esemény objektuma.
   */
  @Override
  public void mouseClicked(MouseEvent e) {
    ;
  }

  /**
   * Egér gombjának lenyomásakor, nyomva tartása alatt keletkező esemény.
   * @param e Keletkező esemény objektuma.
   */
  @Override
  public void mousePressed(MouseEvent e) {
    if (e.getButton() == MouseEvent.BUTTON1) {
        egérBalgombLenyombva = true;
        initThread();
    }else if(e.getButton() == MouseEvent.BUTTON3){
        egérJobbgombLenyombva = true;
        initThread();
    }
  }
  
  /**
   * Egérgomb felengedésekor keletkező esemény lekezelése
   * @param e Keletkező esemény objektuma.
   */  
  @Override
  public void mouseReleased(MouseEvent e) {
    if (e.getButton() == MouseEvent.BUTTON1) {
        egérBalgombLenyombva = false;
    }else if(e.getButton() == MouseEvent.BUTTON3)
        egérJobbgombLenyombva = false;
  }
  
  /**
   * A függvény arra a kérdésre ad választ, hogy indítható-e függvényszámolás.
   * Ha már fut a lekezelése, akkor nem. Ha még nem, akkor foglaltra állítja a 
   * fut nevű szemafort, és engedi a lekezelés indítását.
   * @return true, ha indítható a számolás, és false, ah nem.
   */
  private synchronized boolean checkAndMark() {
    if (fut)
      return false;
    fut = true;
    return true;
  }
  
  /**
   * Zoomolás lekezelése, külön szálban.
   * Lehetne a középső egérgombra vonszolást is megvalósítani itt.
   */
  private void initThread() {
    if (checkAndMark()) {
      new Thread() {
        @Override
        public void run() {
          if(egérBalgombLenyombva){
            do {
              if(System.currentTimeMillis()-előzőIdőpont>100){
                előzőIdőpont=System.currentTimeMillis();
                fraktálModell.setZoom(fraktálModell.getZoom()*0.9);
                setZoom(fraktálModell.getZoom());
                fraktálModell.setXEltolás(fraktálModell.getXEltolás()+
                        egérPozíció.x*(1-0.889)*fraktálModell.getZoom());
                setTfXEltolás(fraktálModell.getXEltolás());
                fraktálModell.setYEltolás(fraktálModell.getYEltolás()+
                        egérPozíció.y*(1-0.889)*fraktálModell.getZoom());
                setTfYEltolás(fraktálModell.getYEltolás());
                MandelbrotSzámoló.init(fraktálModell, fraktálAblak, getFraktálterületMéret().width,
                    getFraktálterületMéret().height,"");
              }
            }while(egérBalgombLenyombva);
          }else{//akkor a jobb gomb van lenyomva
            do {
              if(System.currentTimeMillis()-előzőIdőpont>100){
                előzőIdőpont=System.currentTimeMillis();
                fraktálModell.setXEltolás(fraktálModell.getXEltolás()-
                        egérPozíció.x*(1-0.9)*fraktálModell.getZoom());
                setTfXEltolás(fraktálModell.getXEltolás());
                fraktálModell.setYEltolás(fraktálModell.getYEltolás()-
                        egérPozíció.y*(1-0.9)*fraktálModell.getZoom());
                setTfYEltolás(fraktálModell.getYEltolás());
                fraktálModell.setZoom(fraktálModell.getZoom()*1.1);
                setZoom(fraktálModell.getZoom());
                MandelbrotSzámoló.init(fraktálModell,fraktálAblak, fraktálAblak.getFraktálterületMéret().width,
                    fraktálAblak.getFraktálterületMéret().height,"");
              }
            }while(egérJobbgombLenyombva);
          }
          fut = false;
        }
      }.start();
    }
  }

  /**
   * Csak az interfész miatt kell, de nem használom
   * @param e 
   */
  @Override
  public void mouseEntered(MouseEvent e) {
    ;
  }

  /**
   * Csak az interfész miatt kell, de nem használom
   * @param e 
   */
  @Override
  public void mouseExited(MouseEvent e) {
    ;
  }

  /**
   * Az ablak átméretezését kezeli le.
   * @param e Az átméretezés esemény objektumát tárolja.
   */
  @Override
  public void componentResized(ComponentEvent e) {
    /*if(fraktálAblak.getSize().width<keretMéret.width || fraktálAblak.getSize().height<keretMéret.height){
      fraktálAblak.setSize(keretMéret);
    }*/
    if(!lbSegítség.isShowing()){//ha a segítség látszik akkor átméretezéskor ne számoljon
      setFraktálterületMéret(pnFraktál.getSize());
      if(!MandelbrotSzámoló.isFoglalt())
        MandelbrotSzámoló.init(fraktálModell, fraktálAblak,
                  pnFraktál.getSize().width,pnFraktál.getSize().height, "");
    }
    System.out.println("rajzterület mérete: "+pnFraktál.getSize());
    System.out.println("contentpane mérete átméretezés után: "+getContentPane().getSize());
    System.out.println("eszköztár mérete: "+pnEszköztár.getSize());

  }

  /**
   * Csak az interfész miatt kell, de nem használom
   * @param e 
   */
  @Override
  public void componentMoved(ComponentEvent e) {
    ;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  /**
   * Csak az interfész miatt kell, de nem használom
   * @param e 
   */
  @Override
  public void componentShown(ComponentEvent e) {
    ;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  /**
   * Csak az interfész miatt kell, de nem használom
   * @param e 
   */
  @Override
  public void componentHidden(ComponentEvent e) {
    ;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
}